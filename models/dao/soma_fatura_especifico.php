<?php
$data_igual = $_POST['data_igual'];
/*CONVERTER SEGUNDOS PARA HORAS*/
function segundos_para_hora($segundos) {
  $horas = floor($segundos / 3600);
  $minutos = floor($segundos % 3600 / 60);
  $segundos = $segundos % 60;
  return sprintf("%02d:%02d:%02d", $horas, $minutos, $segundos);
}
/*TOTAL FATURA SIMPLES*/
function total_fat_simples($data_igual) {
include "../controllers/connection.php";
$sql = "SELECT * FROM fat_simples_tabela WHERE fat_data = '$data_igual'";
$query = $mysqli->query($sql) or die ($mysqli->error);
$valor_fatura = 0;
while($info = $query->fetch_assoc()){
   $valor_fatura+=$info['fat_valor']; 
    }
  echo "R$ ".number_format($valor_fatura, 2, ',', '.');
   $mysqli->close();
}
/*TOTAL VALOR FIXO DETALHADO*/
function total_fixo($data_igual){
include "../controllers/connection.php";
$sql = "SELECT * FROM fixo_tabela WHERE fixo_data = '$data_igual'";
$query = $mysqli->query($sql) or die ($mysqli->error);
$valor = 0;
while($info = $query->fetch_assoc()){
    $valor+=$info['fixo_valor'];
    }
    echo "R$ ".number_format($valor, 2, ',', '.');
   $mysqli->close();
}
/*TOTAL VALOR MOVEL DETALHADO*/
function total_movel($data_igual){
include "../controllers/connection.php";
$sql = "SELECT * FROM movel_tabela WHERE movel_data = '$data_igual'";
$query = $mysqli->query($sql) or die ($mysqli->error);
$valor = 0;
while($info = $query->fetch_assoc()){
  $valor+=$info['movel_valor'];
    }
    echo "R$ ".number_format($valor, 2, ',', '.');
   $mysqli->close();
  }
/*TOTAL TEMPO FIXO DETALHADO*/
function total_tempo_fixo($data_igual){
include "../controllers/connection.php";
$sql = "SELECT * FROM fixo_tabela WHERE fixo_data = '$data_igual'";
$query = $mysqli->query($sql) or die ($mysqli->error);
$converter = 0;
$tempo = 0;
while($info = $query->fetch_assoc()){
  list($horas,$minutos,$segundos) = explode(":", $info['fixo_tempo']);
  $converter = ($horas * 3600) + ($minutos * 60) + $segundos;
  $tempo+=$converter;
  }
  echo segundos_para_hora($tempo);
  $mysqli->close();
}
/*TOTAL TEMPO MOVEL DETALHADO*/
function total_tempo_movel($data_igual){
include "../controllers/connection.php";
$sql = "SELECT * FROM movel_tabela WHERE movel_data = '$data_igual'";
$query = $mysqli->query($sql) or die ($mysqli->error);
$converter = 0;
$tempo = 0;
while($info = $query->fetch_assoc()){
  list($horas,$minutos,$segundos) = explode(":", $info['movel_tempo']);
  $converter = ($horas * 3600) + ($minutos * 60) + $segundos;
  $tempo+=$converter;
  }
  echo segundos_para_hora($tempo);
   $mysqli->close();
  }