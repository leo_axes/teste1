<?php
$data_inicial = $_POST['data_inicial'];
$data_final = $_POST['data_final'];

function remover_str($dado){
    $remover = "MAO-AM";
    $resultado = str_replace($remover, "", $dado);
    return $resultado;
}

function tabela_fat_simples($data_inicial, $data_final){
include "../controllers/connection.php";
$sql = "SELECT * FROM fat_simples_tabela WHERE fat_data BETWEEN '$data_inicial' AND '$data_final'";
$query = $mysqli->query($sql) or die ($mysqli->error);
while($info = $query->fetch_assoc()){
    echo "<tr>
    <td>".$info['fat_tipo_servico']."</td>
    <td>".$info['fat_qtd']."</td>
    <td>R&#36; ".number_format($info['fat_valor'], 2, ',', '.')."</td>
    </tr>";   
    }
    $mysqli->close();
}

function tabela_fixo($data_inicial, $data_final){
include "../controllers/connection.php";
$sql = "SELECT * FROM fixo_tabela WHERE fixo_data BETWEEN '$data_inicial' AND '$data_final'";
$query = $mysqli->query($sql) or die ($mysqli->error);
while($info = $query->fetch_assoc()){
    echo "<tr class='table_text_center'>
    <td>".date('d/m/Y', strtotime($info['fixo_data']))."</td>
    <td>".$info['fixo_hora']."</td>
    <td>".remover_str($info['fixo_origem'])."</td>
    <td>".remover_str($info['fixo_destino'])."</td>
    <td>".$info['fixo_tempo']."</td>
    <td>R&#36; ".number_format($info['fixo_valor'], 2, ',', '.')."</td>
    </tr>"; 
    }
    $mysqli->close();
}

// date('d-m-Y', strtotime($info['movel_data']))

function tabela_movel($data_inicial, $data_final){
include "../controllers/connection.php";
$sql = "SELECT * FROM movel_tabela WHERE movel_data BETWEEN '$data_inicial' AND '$data_final'";
$query = $mysqli->query($sql) or die ($mysqli->error);
while($info = $query->fetch_assoc()){
    echo "<tr class='table_text_center'>
    <td>".date('d/m/Y', strtotime($info['movel_data']))."</td>
    <td>".$info['movel_hora']."</td>
    <td>".remover_str($info['movel_origem'])."</td>
    <td>".remover_str($info['movel_destino'])."</td>
    <td>".$info['movel_tempo']."</td>
    <td>R&#36; ".number_format($info['movel_valor'], 2, ',', '.')."</td>
    </tr>"; 
    }
    $mysqli->close();
}


