<?php
function listar_json_fatura_especifico(){
	include "../controllers/connection.php";
	header("Content-Type: text/json");
	$sql = "SELECT * FROM fat_simples_tabela WHERE fat_data = '2018/02/04'";
	$query = $mysqli->query($sql) or die ($mysqli->connect_errno);
	while ($info = $query->fetch_assoc()) {
		$dados = json_encode($info, JSON_PRETTY_PRINT);
		echo $dados;
	}
}
function listar_json_fatura_intervalo(){
	include "../controllers/connection.php";
	header("Content-Type: text/json");
	$sql = "SELECT * FROM fat_simples_tabela WHERE fat_data BETWEEN '2019/03/10' AND '2019/03/20'";
	$query = $mysqli->query($sql) or die ($mysqli->connect_errno);
	$vetor = array();
	while ($info = $query->fetch_assoc()) {
		// $dados = json_encode($info, JSON_PRETTY_PRINT);
		// echo $dados;
		$vetor[] = date('d/m/Y', strtotime($info['fat_data']));
	}
	echo json_encode($vetor,JSON_UNESCAPED_SLASHES);
	// $recebe_dados = array();
	// foreach ($query as $info) {
		
	// 	$recebe_dados[] = $info['fat_tipo_servico'];
	// 	$recebe_dados[] = $info['fat_qtd'];
	// 	$recebe_dados[] = $info['fat_valor'];
	// 	$recebe_dados[] = date('d/m/Y', strtotime($info['fat_data']));
	// }

	// echo json_encode($recebe_dados,JSON_UNESCAPED_SLASHES);

}