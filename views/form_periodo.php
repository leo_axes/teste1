<?php include "../includes/header.php"; ?>
<link rel="stylesheet" href="../css/estilo_periodo.css">
<script type="text/javascript" src="../js/mostrar_form.js"></script>

<div class="modal fade margin_modal" id="myModal">
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Período desejado da fatura</h4>
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
			</div>
			<center>
			<div class="modal-body col-md-6">
				<!-- ESCOLHA MOSTRAR/ESCONDER FORMULARIO -->
				<input type="radio" id="periodo_igual" name="periodo" value="periodo_igual"  onclick="showHideform()">Específico
				<input type="radio" class="margin_radio"id="periodo_intervalo" name="periodo" value="periodo_intervalo" onclick="showHideform()">Intervalo
				<!-- FORMULARIO IGUAL A -->
				<form action="./tabela_especifico.php" method="POST">
				<fieldset id="form_data_igual" class="esconder margin_modal_body">
				Igual a:<input type='date' class='form-control' id='data_igual' name='data_igual' required>
				<div class="modal-footer margin_botao">
				<a href="../index.php" class="btn btn-light">Voltar</a>
				<input type="submit" name="Buscar" value="Buscar" class="btn btn-light"> 
				</div>
				</fieldset>
				</form>
				<!-- FORMULARIO INTERVALO -->
				<form action="./tabela_intervalo.php" method="POST">
				<fieldset id="form_data_intervalo" class="esconder margin_modal_body">
				De:<input type='date' class='form-control' id='data_inicial' name='data_inicial' required>
				Até:<input type='date' class='form-control' id='data_final' name='data_final' required>
				<div class="modal-footer margin_botao">
				<a href="../index.php" class="btn btn-light">Voltar</a>
				<a href=""><input type="submit" name="Buscar" value="Buscar" class="btn btn-light"></a> 
				</div>
				</fieldset>
				</form>
				</div>
				</center>
		</div>
	</div>
</div>
<!-- 	<iframe src="../img/logo_axes_talk.png" width="600" height="780" style="border: none"></iframe> -->
<?php include "../includes/footer.php"; ?>
<script type="text/javascript" src="../js/modal_periodo.js"></script>
