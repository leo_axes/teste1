CREATE TABLE `movel_tabela` (
 `movel_id` int(11) NOT NULL AUTO_INCREMENT,
 `movel_data` date NOT NULL,
 `movel_hora` time NOT NULL,
 `movel_origem` varchar(50) COLLATE utf8_bin NOT NULL,
 `movel_destino` varchar(50) COLLATE utf8_bin NOT NULL,
 `movel_tempo` time NOT NULL,
 `movel_valor` int(11) NOT NULL,
 PRIMARY KEY (`movel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin