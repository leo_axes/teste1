<?php include "../includes/header.php"; ?>
<?php include "../models/dao/listar_especifico.php"; ?>
<?php include "../models/dao/soma_fatura_especifico.php"; ?>
<link rel="stylesheet" href="../css/estilo_tabela.css">

<div class="conteiner bg-light margin_conteiner">
	<div class="row">
		<div class="col-1"></div>
		<div class="col-4">
				<ul class="retira_pontos_lista">
					<li><a href="../index.php"><img src="../img/logo_central.png" width="200" height="50"></a></li>
				</ul>	
				<ul class="retira_pontos_lista w-100 margin_informacoes_1 fonte_head">
					<li><b>AXES TECNOLOGIA</b></li>
					<li>Av. Brasil, 1000, Santo Agostinho</li>
					<li>92 3090-3090</li>
					<li>axes.com.br</li>
				</ul>
		</div>
  		<div class="col-2"></div>
    	<div class="col-4">
    		<ul class="retira_pontos_lista margin_informacoes_2 form-control-plaintext fonte_head">
    			
	    		<li><b>Data de Emissão:</b> 22/03/2019</li>
	    		<li><b>Titular da conta:</b> Moisés Pereira de Oliveira</li>
		    	<li><b>Nº Celular:</b> 92 994562828</li>
		    	<li><b>Nº de Contrato:</b> 511486670</li>
		    	<li><b>Período de referência:</b> 04/02/2018</li>
    		</ul>
    	</div>
    	<div class="col-1"></div>
	</div>
</div>
<img src="../img/logo_axes_talk.png" width="150" height="50" class="logo_talk">
<!-- TABELA FAT SIMPLES -->
<div class="conteiner table_margin_fat_detalhada">
	<div class="row">
<div class="col"></div>
	<div class="col-10">
	<table class="table table-hover"> <!-- table-bordered = tabela com bordas -->
	<thead>
		<tr>
			<th>Tipo de Serviço</th>
			<th>Quantidade</th>
			<th>Valor</th>
		</tr>
	</thead>
	<tbody>
        <?php 
        tabela_fat_simples($data_igual);
        ?>
	</tbody>
<!-- TOTAL FAT SIMPLES -->
	<thead class="bg-primary cor_texto_branco">
		<tr>
			<th colspan="2">Total da Fatura</th>
			<th><?php total_fat_simples($data_igual); ?></th>
		</tr>
	</thead>
	</table>
	</div>
	<div class="col"></div>
	</div>
</div>
<!-- TABELA TALK FIXO -->
<div class="conteiner table_margin_fat_detalhada">
	<div class="row">
		<h4 class="label_talk_detalhado">TALK FIXO</h4>
	</div>
	<div class="row">
		<div class="col"></div>
		<div class="col-md-10">
			<table class="table table-hover"> <!-- table-bordered = tabela com bordas -->
				<thead>
				<tr class="table_text_center">
					<th>Data</th>
					<th>Hora</th>
					<th>Origem</th>
					<th>Destino</th>
					<th>Tempo</th>
					<th>Valor</th>			
				</tr>
				</thead>
				<tbody>
					<?php
			    	tabela_fixo($data_igual); 
			    	?>
				</tbody>
			<!-- TOTAL FAT FIXO -->
				<thead>
				<tr class="bg-light">
					<th colspan="4">Total</th>
					<th><?php total_tempo_fixo($data_igual); ?></th>
					<th><?php total_fixo($data_igual); ?></th>
				</tr>
				</thead>
			</table>
	</div>
		<div class="col"></div>
	</div>
</div>
<!-- TABELA TALK MOVEL -->
<div class="conteiner table_margin_fat_detalhada">
	<div class="row">
		<h4 class="label_talk_detalhado">TALK MOVEL</h4>
	</div>
	<div class="row">
		<div class="col"></div>
		<div class="col-md-10">
			<table class="table table-hover"> <!-- table-bordered = tabela com bordas -->
				<thead>
				<tr class="table_text_center">
					<th>Data</th>
					<th>Hora</th>
					<th>Origem</th>
					<th>Destino</th>
					<th>Tempo</th>
					<th>Valor</th>
				</tr>
				</thead>
				<tbody>
					<?php 
					tabela_movel($data_igual);
		        	?>
				</tbody>
		<!-- TOTAL FAT MOVEL -->
				<thead>
				<tr class="bg-light">
					<th colspan="4">Total</th>
					<th><?php total_tempo_movel($data_igual); ?></th>
					<th><?php total_movel($data_igual); ?></th>
				</tr>
				</thead>
		<!-- TOTAL FAT SIMPLES -->
				<thead>
					<tr class="bg-primary cor_texto_branco">
						<th colspan="5">Total da Fatura</th>
						<th><?php total_fat_simples($data_igual); ?></th>
					</tr>
				</thead>
			</table>
	</div>
		<div class="col"></div>
	</div>
</div>
<center>	
	<div class="table_margin_botoes print">
		<a href="../index.php" class="btn "><i class="fas fa-arrow-left  fa-2x"></i></a>
		<a href="javascript:print();" class="btn" ><i class="fas fa-print fa-2x"></i></a>
		<!-- <a href="javascript:teste()" class="btn" ><i class="far fa-file-pdf"></i></a> -->
	</div>
</center>
<?php include "../includes/footer.php"; ?>